# System imports

# libs imports
from pydantic import BaseModel
from fastapi import APIRouter, responses

# local imports

router = APIRouter()


class CarType(BaseModel):
    id: int
    mark: str
    model: str
    car_door_number: int
    engine: str


cartypes = [
    {"id": 1, "mark": "Peugeot", "model": "208",
        "car_door_number": 5, "engine": "1.2L"},
    {"id": 2, "mark": "Peugeot", "model": "308",
        "car_door_number": 5, "engine": "1.6L"},
    {"id": 3, "mark": "Peugeot", "model": "508",
        "car_door_number": 5, "engine": "2.0L"},
    {"id": 4, "mark": "Renault", "model": "Clio",
        "car_door_number": 5, "engine": "1.2L"},
    {"id": 5, "mark": "Renault", "model": "Megane",
        "car_door_number": 5, "engine": "1.6L"},
]


@router.get("/cartypes", response_model_exclude_unset=True)
async def get_all_cartypes() -> list[CarType]:
    return cartypes


@router.get("/cartypes/search")
async def search_cartypes(mark: str) -> list[CarType]:
    return list(filter(lambda x: x["mark"] == mark, cartypes))


@router.get("/cartypes/{cartype_id}")
async def get_cartype_by_id(cartype_id: int, mark: str | None = None) -> list[CarType]:
    filtred_list = list(filter(lambda x: x["id"] == cartype_id, cartypes))
    if mark is not None:
        filtred_list = list(filter(lambda x: x["mark"] == mark, cartypes))
    return filtred_list


@router.post("/cartypes")
async def create_cartype(cartype: CarType):
    cartypes.append(cartype)
    return cartype


@router.delete("/cartypes/delete/{cartype_id}")
async def delete_cartype(cartype_id: int):
    del cartypes[cartype_id-1]
    return cartypes
