# System imports
from enum import Enum

# libs imports
from pydantic import BaseModel
from fastapi import APIRouter, responses


# local imports
from models.user import User
from models.cartype import CarType


router = APIRouter()


class Car(BaseModel):
    id: int
    cartype: CarType
    kilometers: int
    color: Enum
    retail_price: float
    purchase_price: float
    options: Enum
    state: Enum
    construction_year: int
    parking_spot: int
    arrival_date: str
    delivry_date: str
    sales_employee: User
    preview_owner: User
    new_owner: User


class color(Enum):
    RED = "red"
    BLUE = "blue"
    GREEN = "green"
    YELLOW = "yellow"
    BLACK = "black"
    WHITE = "white"
    PINK = "pink"


class options(Enum):
    AC = "air conditioning"
    HEATING_SEATS = "heating seats"


class state(Enum):
    NEW = "new"
    USED = "second hand"
    RECONDITIONED = "reconditioned"


cars = [
    {"id": 1, "cartype": 1, "kilometers": 10000, "color": "red", "retail_price": 20000, "purchase_price": 15000, "options": "air conditioning", "state": "new",
        "construction_year": 2021, "parking_spot": 1, "arrival_date": "2021-01-01", "delivry_date": "2021-01-01", "sales_employee": 1, "preview_owner": 1, "new_owner": 1},
    {"id": 2, "cartype": 2, "kilometers": 10000, "color": "red", "retail_price": 20000, "purchase_price": 15000, "options": "air conditioning", "state": "new",
        "construction_year": 2021, "parking_spot": 1, "arrival_date": "2021-01-01", "delivry_date": "2021-01-01", "sales_employee": 1, "preview_owner": 1, "new_owner": 1},
    {"id": 3, "cartype": 3, "kilometers": 10000, "color": "red", "retail_price": 20000, "purchase_price": 15000, "options": "air conditioning", "state": "new",
        "construction_year": 2021, "parking_spot": 1, "arrival_date": "2021-01-01", "delivry_date": "2021-01-01", "sales_employee": 1, "preview_owner": 1, "new_owner": 1},
]


@router.get("/cars", response_model_exclude_unset=True)
async def get_all_cars() -> list[Car]:
    return cars


@router.get("/cars/search")
async def search_cars(state: str) -> list[Car]:
    return list(filter(lambda x: x["state"] == state, cars))


@router.get("/cars/{car_id}")
async def get_car_by_id(car_id: int, color: str | None = None) -> list[Car]:
    filtred_list = list(filter(lambda x: x["id"] == car_id, cars))
    if color is not None:
        filtred_list = list(filter(lambda x: x["color"] == color, cars))
    return filtred_list


@router.post("/cars")
async def create_car(car: Car):
    cars.append(car)
    return car


@router.delete("/cars/delete/{car_id}")
async def delete_car(car_id: int):
    del cars[car_id-1]
    return cars
