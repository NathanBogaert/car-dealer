# System imports

# libs imports
from pydantic import BaseModel
from fastapi import APIRouter, responses
from fastapi.encoders import jsonable_encoder

# local imports

router = APIRouter()


class User(BaseModel):
    id: int
    name: str
    surname: str
    email: str
    password_hash: str
    tel: int | None = None
    newslatter: bool
    is_client: bool


users = [
    {"id": 1, "name": "Hakim", "surname": "Bachabi",
        "email": "hakim.bachabi@email.com", "password_hash": "b7e507f7b30caff568e11c613de215eba2f861b8545ef8c30298fdf9ddcd97e8", "password": "bambi", "tel": 123456789, "newslatter": True, "is_client": True},
    {"id": 2, "name": "Nathan", "surname": "Bogaert",
        "email": "nathan.bogaert@email.com", "password_hash": "a08edd0d5d90db17d422ca1e31734c8d6485e23ae0dfa44ab61e7e67f77f4983", "password": "jonnDoe", "tel": 123456789, "newslatter": True, "is_client": False},
    {"id": 3, "name": "Jacques", "surname": "Lepré",
        "email": "jacques.lepre@email.com", "password_hash": "81fdff283ec2829b4002384ad18370f64e7a48618c45058e3d112d965e27f72e", "password": "casper", "tel": 123456789, "newslatter": False, "is_client": True},
]


@router.get("/users", response_model_exclude_unset=True)
async def get_all_users() -> list[User]:
    return users


@router.get("/users/search")
async def search_users(name: str) -> list[User]:
    return list(filter(lambda x: x["name"] == name, users))


@router.get("/users/{user_id}")
async def get_user_by_id(user_id: int, name: str | None = None) -> list[User]:
    filtred_list = list(filter(lambda x: x["id"] == user_id, users))
    if name is not None:
        filtred_list = list(filter(lambda x: x["name"] == name, users))
    return filtred_list


@router.post("/users")
async def create_user(user: User):
    users.append(user)
    return user


@router.delete("/users/delete/{user_id}")
async def delete_user(user_id: int):
    del users[user_id-1]
    return users


@router.put("/users/update/{user_id}")
async def update_item(user_id: int, user: User):
    update_user_json = jsonable_encoder(user)
    if users[user_id-1] is not None:
        del users[user_id-1]
    users[user_id-1] = update_user_json
    return update_user_json


@router.patch("/users/patch/{user_id}")
async def patch_item(user_id: int, user: User):
    stored_users_data = users[user_id-1]
    stored_users_model = User(**stored_users_data)
    update_data = user.dict(exclude_unset=True)
    update_users = stored_users_model.copy(update=update_data)
    if users[user_id-1] is not None:
        del users[user_id-1]
    users[user_id-1] = jsonable_encoder(user)
    return update_users
