# System imports

# libs imports
from fastapi import FastAPI, status, Depends

# local imports
from models import car, cartype, user
from internal import auth


app = FastAPI()


@app.get("/")
async def say_hello():
    """Ceci est une fonction qui dit bonjour oulala
    """
    return "bonjour"

app.include_router(car.router, tags=["cars"])
app.include_router(cartype.router, tags=["cartypes"])
app.include_router(user.router, tags=["users"])
app.include_router(auth.router, tags=["auth"])
